# ESP32_Owon_B35T_BLE_MQTT_Gateway

Using the BLE function of the ESP32 to collect the published data from a Owon B35T digital Multimeter.
The values are sent to a MQTT Broker, processed by Node-RED and saved to a InfluxDB database. 


![Grafana](img/grafana.JPG "Grafana")

After that they can be displayed with Grafana and the provided Dashboard.

## Setup
1. Edit the sketch to match your WIFI and Server settings  
2. Compile the Arduino Sketch for  ESP32 board (use partition scheme: minimal_spiffs) and upload it to the uC
3. Create a new InfluxDB Database or use exisiting
4. Import the Node-RED flow, edit the InfluxDB Node and deploy it
5. Import the Grafana Dashboard and edit the Datasource 

## Credits
BLE communication with the DMM is used from [This awesome Project ](https://github.com/reaper7/M5Stack_BLE_client_Owon_B35T)!
