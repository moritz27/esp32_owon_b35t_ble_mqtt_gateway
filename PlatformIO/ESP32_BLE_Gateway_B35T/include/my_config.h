#include "secrets.h"

/* ########################## PINS ########################## */
#if defined(ESP8266)
#define SDA_PIN D3
#define SCL_PIN D4
#elif defined(ESP32)
#ifndef SDA_PIN
#define SDA_PIN 21 // 16 //
#endif
#ifndef SCL_PIN
#define SCL_PIN 22 // 17 //
#endif
#endif

/* ########################## OTA ########################### */
#define OTA


/* ########################## WIFI ########################## */
#ifndef MY_SSID
#define MY_SSID "YourSSID"
#endif
#ifndef MY_WIFI_PASSWORD
#define MY_WIFI_PASSWORD "YourPassword"
#endif

/* ########################## MQTT ########################## */
#define MQTT
#ifdef MQTT
#ifndef MQTT_SERVER
#define MQTT_SERVER "YourMqttServerIP"
#endif
#ifndef MQTT_TOPIC
#define MQTT_TOPIC "B35T"
#endif
#ifndef MY_HOSTNAME
#define MY_HOSTNAME "owon_dmm_gateway"
#endif
#endif

/* ########################## user variables ########################## */

#ifndef SLEEP_INTERVAL
#define SLEEP_INTERVAL 10E6 // 10 seconds in microseconds, max: 71 minutes
// #define SLEEP_INTERVAL 5*60E6 //5 minutes in microseconds, max: 71 minutes
#endif

#ifndef RETRY_NUM
#define RETRY_NUM 5
#endif