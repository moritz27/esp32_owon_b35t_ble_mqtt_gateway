#include <Arduino.h>
#include <ArduinoJson.h>
#include "my_debug.h"

#define OWONNAME "BDM"                                              // OWON device name
#define serviceUUID "0000fff0-0000-1000-8000-00805f9b34fb"          // OWON service UUID
#define charnotificationUUID "0000fff4-0000-1000-8000-00805f9b34fb" // OWON notification characteristic UUID
#define charwriteUUID "0000fff3-0000-1000-8000-00805f9b34fb"        // OWON write characteristic UUID
#define replySize 14                                                // number of bytes in meter reply

void initValues(DynamicJsonDocument &json);
void checkUnit(uint8_t replyBuffer, DynamicJsonDocument &json);
void checkScale(uint8_t replyBuffer, DynamicJsonDocument &json);
void checkMeasMode(uint8_t replyBuffer, DynamicJsonDocument &json);
void checkMinMaxHold(uint8_t replyBuffer, DynamicJsonDocument &json);
float convertValue(uint8_t replyBuffer[]);
float normalizeValue(float measurement, String usedScale);
void parseValues(uint8_t replyBuffer[], DynamicJsonDocument &json);