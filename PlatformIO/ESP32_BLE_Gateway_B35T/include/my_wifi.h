#if defined(ESP8266)
  #include <ESP8266WiFi.h>
#elif defined(ESP32)
  #include <WiFi.h>
#else
  #error "This ain't a ESP8266 or ESP32, dumbo!"
#endif

#include "my_debug.h"

void setup_wifi(WiFiClient& wifi_client, const char* ssid, const char* wifi_password, const uint8_t retries = 10);