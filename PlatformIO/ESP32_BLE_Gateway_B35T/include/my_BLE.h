#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEAdvertisedDevice.h>
#include <BLEClient.h>
#include <BLEScan.h>
#include <BLEUtils.h>
#include "my_debug.h"
#include "my_B35T.h"

#define maxWaitForNotify 10000 // max wait time for next notify from meter in ms
#define scanTime 10            // BLE scan time in s

static void notifyCallback(BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify);
bool connectToServer();
void doScan();
bool getConnectionStatus();
bool newBleDataAvailable();
void resetBleDataAvailable();
unsigned long getLastBleNotifyTime();
void setLastBleNotifyTime(unsigned long value);
unsigned long getScanStartTime();
void setScan(bool status);
uint8_t* getReplyBuffer();
