#include <Arduino.h>
#include "my_debug.h"

void dynamic_sleep(const uint32_t sleep_interval, uint32_t send_timestamp = 0);