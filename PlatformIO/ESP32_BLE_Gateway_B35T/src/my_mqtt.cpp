#include "my_mqtt.h"

void setup_mqtt(PubSubClient& client, const char* hostname, const uint8_t retries) {
  DEBUG_MSG("Setup MQTT \n");
  int success = false;
  for (int i = 0; i <= retries; i++) {
    client.connect(hostname);
    if (client.connected()) {
      success = true;
      break;
    }
    if (success == true) {
      DEBUG_MSG("MQTT connected \n");
    }
    else {
      DEBUG_MSG("MQTT could not be connected, restarting now \n");
      delay(100);
      ESP.restart();
      delay(100);
    }
  }
  return;
}

uint32_t send_mqtt_string(PubSubClient& client, const char* hostname, const char* content, const char* topic, const uint8_t retries) {
  if (!client.connected()) {
    // DEBUG_MSG("MQTT disconnected, reconnect now \n");
    client.connect(hostname);
    delay(100);
  }
  int success = false;
  for (int i = 0; i <= retries; i++) {
    DEBUG_MSG("trying to send topic: %S \n", topic);
    if (client.publish(topic, content)) {
      success = true;
      break;
    }
    else {
      client.connect(hostname);
      delay(100);
    }
    delay(100);
  }
  if (success == true) {
    DEBUG_MSG("%S message sent \n", topic);
    return micros();
  }
  else {
    // DEBUG_MSG("%S message could not be sent, restarting now \n" , topic);
    delay(100);
    ESP.restart();
    delay(100);
    return 0;
  }
}

uint32_t send_mqtt_json(PubSubClient& client, const char* hostname, DynamicJsonDocument& json, const char* topic, const uint8_t retries) {
    String json_string;
    serializeJson(json, json_string);
    return send_mqtt_string(client, hostname, json_string.c_str(), topic);
}

