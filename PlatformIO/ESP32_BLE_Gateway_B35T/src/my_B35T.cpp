#include "my_B35T.h"
#include <ArduinoJson.h>

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*
  14 bytes reply
  -----------------------------------------------------------------------
  |  1 |         2         |  3 |  4 |  5 |  6 |    7    |  8 |    9    |
  -----------------------------------------------------------------------
  |0x2b|0x33 0x36 0x32 0x33|0x20|0x34|0x31|0x00|0x40 0x80|0x24|0x0d 0x0a|
  -----------------------------------------------------------------------
  |  0 |  1    2    3    4 |  5 |  6 |  7 |  8 |  9   10 | 11 | 12   13 |
  -----------------------------------------------------------------------
*/

/*
  1:  + or - (dec 43 or 45)
    BYTE 0
*/
#define REGPLUSMINUS 0x00
#define FLAGPLUS B00101011
#define FLAGMINUS B00101101

/*
  2:  Value 0000-9999 in dec
    BYTE 1-4
*/
#define REGDIG1 0x01
#define REGDIG2 0x02
#define REGDIG3 0x03
#define REGDIG4 0x04

/*
  3:  Just space (dec 32)
    BYTE 5
*/

/*
  4:  Decimal point position
    - dec 48 no point
    - dec 49 after the first number
    - dec 50 after the second number
    - dec 52 after the third number
    BYTE 6
*/
#define REGPOINT 0x06
#define FLAGPOINT0 B00110000
#define FLAGPOINT1 B00110001
#define FLAGPOINT2 B00110010
#define FLAGPOINT3 B00110100

/*
  5:  AC or DC and Auto mode
    - dec 49 DC Auto mode / 51 HOLD
    - dec 41 AC Auto mode / 43 HOLD
    - dec 17 DC Manual mode / 19 HOLD
    - dec 09 AC Manual mode / 11 HOLD
    BYTE 7
*/
#define REGMODE 0x07
#define FLAGMODENONE B00000000  // none
#define FLAGMODEMAN B00000001   // manual
#define FLAGMODEHOLD B00000010  // hold
#define FLAGMODEREL B00000100   // relative
#define FLAGMODEAC B00001000    // ac
#define FLAGMODEDC B00010000    // dc
#define FLAGMODEAUTO B00100000  // auto
#define FLAGMODECHECK B11111111 // mask for check others

/*
  6:  MIN MAX
    - dec  0 MIN MAX off
    - dec 16 MIN
    - dec 32 MAX
    BYTE 8
*/
#define REGMINMAX 0x08
#define FLAGMINMAXNONE B00000000
#define FLAGMIN B00010000
#define FLAGMAX B00100000

/*
  7:  Units
    - dec   2   0    % Duty
    - dec   0   1 Fahrenheit
    - dec   0   2 Grad
    - dec   0   4   nF
    - dec   0   8   Hz
    - dec   0  16  hFE
    - dec   0  32  Ohm
    - dec  32  32 kOhm
    - dec  16  32 MOhm
    - dec 128  64   uA
    - dec  64  64   mA
    - dec   0  64    A
    - dec  64 128   mV
    - dec   0 128    V
    BYTES 9 & 10
*/
#define REGSCALE 0x09
#define FLAGSCALEDUTY B00000010
#define FLAGSCALEDIODE B00000100
#define FLAGSCALEBUZZ B00001000
#define FLAGSCALEMEGA B00010000
#define FLAGSCALEKILO B00100000
#define FLAGSCALEMILI B01000000
#define FLAGSCALEMICRO B10000000

#define REGUNIT 0x0a
#define FLAGUNITNONE B00000000
#define FLAGUNITFAHR B00000001
#define FLAGUNITGRAD B00000010
#define FLAGUNITNF B00000100
#define FLAGUNITHZ B00001000
#define FLAGUNITHFE B00010000
#define FLAGUNITOHM B00100000
#define FLAGUNITAMP B01000000
#define FLAGUNITVOLT B10000000

/*
  8:  ???
*/

/*
  9:  CR + LF
*/
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

uint8_t lastValues[replySize - 3];

void initValues(DynamicJsonDocument &json){
  json["value"] = 0.0;
  json["scale"] = "";
  json["unit"] = "V";
  json["capture"] = "Auto";
  json["coupling"] = "AC";
  json["minMaxHold"] = "Off";
}

void checkUnit(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  if (lastValues[REGUNIT] != replyBuffer)
  {
    lastValues[REGUNIT] = replyBuffer;
    switch (lastValues[REGUNIT])
    {
    case FLAGUNITFAHR:
      json["unit"] = "*F";
      break;
    case FLAGUNITGRAD:
      json["unit"] = "*C";
      break;
    case FLAGUNITNF:
      json["unit"] = "nF";
      break;
    case FLAGUNITHZ:
      json["unit"] = "Hz";
      break;
    case FLAGUNITHFE:
      json["unit"] = "hFE";
      break;
    case FLAGUNITOHM:
      json["unit"] = "Ohm";
      break;
    case FLAGUNITAMP:
      json["unit"] = "A";
      break;
    case FLAGUNITVOLT:
      json["unit"] = "V";
      break;
    }
    DEBUG_MSG("Unit changed to: %S", (const char *)json["unit"]);
  }
  return;
}

void checkScale(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  if (lastValues[REGSCALE] != replyBuffer)
  {
    json["scale"] = "";
    if ((replyBuffer & FLAGSCALEDUTY) == FLAGSCALEDUTY)
    {
      lastValues[REGSCALE] |= FLAGSCALEDUTY;
      json["scale"] = "%";
    }
    else
    {
      lastValues[REGSCALE] &= ~(FLAGSCALEDUTY);
    }

    if ((replyBuffer & FLAGSCALEDIODE) == FLAGSCALEDIODE)
      lastValues[REGSCALE] |= FLAGSCALEDIODE;
    else
      lastValues[REGSCALE] &= ~(FLAGSCALEDIODE);

    if ((replyBuffer & FLAGSCALEBUZZ) == FLAGSCALEBUZZ)
    {
      lastValues[REGSCALE] |= FLAGSCALEBUZZ;
    }
    else
    {
      lastValues[REGSCALE] &= ~(FLAGSCALEBUZZ);
    }

    if ((replyBuffer & FLAGSCALEMEGA) == FLAGSCALEMEGA)
    {
      lastValues[REGSCALE] |= FLAGSCALEMEGA;
      json["scale"] = "M";
    }
    else
    {
      lastValues[REGSCALE] &= ~(FLAGSCALEMEGA);
    }

    if ((replyBuffer & FLAGSCALEKILO) == FLAGSCALEKILO)
    {
      lastValues[REGSCALE] |= FLAGSCALEKILO;
      json["scale"] = "k";
    }
    else
    {
      lastValues[REGSCALE] &= ~(FLAGSCALEKILO);
    }

    if ((replyBuffer & FLAGSCALEMILI) == FLAGSCALEMILI)
    {
      lastValues[REGSCALE] |= FLAGSCALEMILI;
      json["scale"] = "m";
    }
    else
    {
      lastValues[REGSCALE] &= ~(FLAGSCALEMILI);
    }

    if ((replyBuffer & FLAGSCALEMICRO) == FLAGSCALEMICRO)
    {
      lastValues[REGSCALE] |= FLAGSCALEMICRO;
      json["scale"] = "u";
    }
    else
    {
      lastValues[REGSCALE] &= ~(FLAGSCALEMICRO);
    }
    DEBUG_MSG("Scale changed to: %S", (const char *)json["scale"]);
  }
  return;
}

void checkMeasMode(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  if (lastValues[REGMODE] != replyBuffer)
  {
    if ((lastValues[REGMODE] & FLAGMODEAUTO) != (replyBuffer & FLAGMODEAUTO))
    { // auto
      if ((replyBuffer & FLAGMODEAUTO) == FLAGMODEAUTO)
      {
        lastValues[REGMODE] |= FLAGMODEAUTO;
        json["capture"] = "Auto";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEAUTO);
      }
      DEBUG_MSG("Auto Mode: %s\n", (lastValues[REGMODE] & FLAGMODEAUTO) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEHOLD) != (replyBuffer & FLAGMODEHOLD))
    { // hold
      if ((replyBuffer & FLAGMODEHOLD) == FLAGMODEHOLD)
      {
        lastValues[REGMODE] |= FLAGMODEHOLD;
        json["capture"] = "Hold";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEHOLD);
      }
      DEBUG_MSG("Hold: %s\n", (lastValues[REGMODE] & FLAGMODEHOLD) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEREL) != (replyBuffer & FLAGMODEREL))
    { // relative
      if ((replyBuffer & FLAGMODEREL) == FLAGMODEREL)
      {
        lastValues[REGMODE] |= FLAGMODEREL;
        json["capture"] = "Relative";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEREL);
      }
      DEBUG_MSG("Relative: %s\n", (lastValues[REGMODE] & FLAGMODEREL) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEDC) != (replyBuffer & FLAGMODEDC))
    { // dc
      if ((replyBuffer & FLAGMODEDC) == FLAGMODEDC)
      {
        lastValues[REGMODE] |= FLAGMODEDC;
        json["coupling"] = "DC";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEDC);
      }
      DEBUG_MSG("DC: %s\n", (lastValues[REGMODE] & FLAGMODEDC) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEAC) != (replyBuffer & FLAGMODEAC))
    { // ac
      if ((replyBuffer & FLAGMODEAC) == FLAGMODEAC)
      {
        lastValues[REGMODE] |= FLAGMODEAC;
        json["coupling"] = "AC";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEAC);
      }
      DEBUG_MSG("AC: %s\n", (lastValues[REGMODE] & FLAGMODEAC) ? "true" : "false");
    }

    DEBUG_MSG("Mode changed to: %S %S \n", (const char *)json["capture"], (const char *)json["coupling"]);
    return;
  }
}

void checkMinMaxHold(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  if (lastValues[REGMINMAX] != replyBuffer)
  {
    json["minMaxHold"] = "Off";
    if ((replyBuffer & FLAGMAX) == FLAGMAX)
    {
      lastValues[REGMINMAX] |= FLAGMAX;
      DEBUG_MSG("Max Hold: %s\n", (lastValues[REGMINMAX] & FLAGMAX) ? "true" : "false");
      json["minMaxHold"] = "Max";
    }
    else
    {
      lastValues[REGMINMAX] &= ~(FLAGMAX);
    }
    if ((replyBuffer & FLAGMIN) == FLAGMIN)
    {
      lastValues[REGMINMAX] |= FLAGMIN;
      DEBUG_MSG("Min Hold: %s\n", (lastValues[REGMINMAX] & FLAGMIN) ? "true" : "false");
      json["minMaxHold"] = "Min";
    }
    else
    {
      lastValues[REGMINMAX] &= ~(FLAGMIN);
    }
    DEBUG_MSG("Hold changed to: %S \n", (const char *)json["minMaxHold"]);
  }
  return;
}

float convertValue(uint8_t replyBuffer[])
{
  char digits[6] = {0, 0, 0, 0, 0, 0};
  uint8_t digitOffset = 0;

  if (lastValues[REGPLUSMINUS] != replyBuffer[REGPLUSMINUS])
  { // sign "-" : 0
    lastValues[REGPLUSMINUS] = replyBuffer[REGPLUSMINUS];
  }
  if ((lastValues[REGPLUSMINUS] & FLAGMINUS) == FLAGMINUS)
  {
    digits[0] = '-';
    digitOffset++;
  }

  if (lastValues[REGDIG1] != replyBuffer[REGDIG1])
  { // digit 1 : 0/1
    lastValues[REGDIG1]  = replyBuffer[REGDIG1];
  }
  if (lastValues[REGDIG1]  >= 0x30 && lastValues[REGDIG1]  <= 0x39)
  {
    digits[0 + digitOffset] = lastValues[REGDIG1];
  }

  if (lastValues[REGPOINT]  != replyBuffer[REGPOINT])
  { // decimal point
    lastValues[REGPOINT]  = replyBuffer[REGPOINT];
  }

  if ((lastValues[REGPOINT]  & FLAGPOINT1) == FLAGPOINT1)
  { // decimal point 1 : 1/2
    digits[1 + digitOffset] = '.';
    digitOffset++;
  }

  if (lastValues[REGDIG2] != replyBuffer[REGDIG2])
  {
    lastValues[REGDIG2]  = replyBuffer[REGDIG2];
  }
  if (lastValues[REGDIG2]  >= 0x30 && lastValues[REGDIG2]  <= 0x39)
  {
    digits[1 + digitOffset] = lastValues[REGDIG2] ; // digit 2 : 1/2/3
  }

  if (((lastValues[REGPOINT] & FLAGPOINT2) == FLAGPOINT2) && (digitOffset <= 1))
  { // decimal point 2 : 2/3
    digits[2 + digitOffset] = '.';
    digitOffset++;
  }

  if (lastValues[REGDIG3] != replyBuffer[REGDIG3])
  {
    lastValues[REGDIG3] = replyBuffer[REGDIG3];
  }
  if (lastValues[REGDIG3] >= 0x30 && lastValues[REGDIG3] <= 0x39)
  {
    digits[2 + digitOffset] = lastValues[REGDIG3]; // digit 3 : 2/3/4
  }

  if (((lastValues[REGPOINT] & FLAGPOINT3) == FLAGPOINT3) && (digitOffset <= 1))
  { // decimal point 3/4
    digits[3 + digitOffset] = '.';
    digitOffset++;
  }

  if (lastValues[REGDIG4] != replyBuffer[REGDIG4])
  {
    lastValues[REGDIG4] = replyBuffer[REGDIG4];
  }

  if (lastValues[REGDIG4] >= 0x30 && lastValues[REGDIG4] <= 0x39)
  {
    digits[3 + digitOffset] = lastValues[REGDIG4]; // digit 3/4/5
  }

  float numValue = atof(digits);
  return numValue;
}

float normalizeValue(float measurement, String usedScale)
{
  if (usedScale == "M")
  {
    return measurement * 1e6;
  }
  else if (usedScale == "k")
  {
    return measurement * 1e3;
  }
  else if (usedScale == "m")
  {
    return measurement / 1e3;
  }
  else if (usedScale == "u")
  {
    return measurement / 1e6;
  }
  else
  {
    return measurement;
  }
}

void parseValues(uint8_t replyBuffer[], DynamicJsonDocument &json)
{
  checkScale(replyBuffer[REGSCALE], json);
  checkUnit(replyBuffer[REGUNIT], json);
  checkMeasMode(replyBuffer[REGMODE], json);
  checkMinMaxHold(replyBuffer[REGMINMAX], json);
  float measuredValue = convertValue(replyBuffer);
  json["value"] = normalizeValue(measuredValue, (const char *)json["scale"]);

  Serial.printf("%4.3f %s%s %s in %s Mode, holding: %s \n", measuredValue, (const char *)json["scale"], (const char *)json["unit"], (const char *)json["coupling"], (const char *)json["capture"], (const char *)json["minMaxHold"]);
  // DEBUG_MSG("%4.3f %S \n", json["value"], json["unit"]);
}