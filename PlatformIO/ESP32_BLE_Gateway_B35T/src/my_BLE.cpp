#include "my_BLE.h"

/* ########################## OWON B35T ########################## */
static BLEAddress *pServerAddress;
static BLERemoteCharacteristic *pRemoteCharacteristicNotify;
static BLERemoteCharacteristic *pRemoteCharacteristicWrite;

volatile boolean deviceBleConnected = false; // flag BLE connected
volatile boolean newBleData = false;         // flag "we get new data from meter"
static unsigned long lastBleNotify = 0;      // timestamp "last received data from meter" in ms
static unsigned long startBleScanning = 0;   // timestamp when ble scan is beginning in ms

uint8_t replyBuffer[replySize]; // meter reply buffer

static void notifyCallback(BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify)
{

  if (isNotify == true && length == replySize && pBLERemoteCharacteristic->getUUID().equals(BLEUUID(charnotificationUUID)))
  {

    //    DEBUG_MSG("I: Notify callback len=%d (UUID: %s)\n", length, pBLERemoteCharacteristic->getUUID().toString().c_str());

    if (memcmp(replyBuffer, pData, replySize) != 0)
    { // if new data <> old data
      if (newBleData == false)
      { // and if old data are displayed then copy new data
        memcpy(replyBuffer, pData, replySize);
        /*
                for (uint8_t i = 0; i < replySize; i++) {
                  DEBUG_MSG("%02X ", replyBuffer[i]);
                }
                DEBUG_MSG("\n");
        */
        newBleData = true;
      }
    }
    lastBleNotify = millis();
  }
}

class MyClientCallbacks : public BLEClientCallbacks
{
  void onConnect(BLEClient *pClient)
  {
    deviceBleConnected = true; // set ble connected flag
    DEBUG_MSG("I: %S connected \n", OWONNAME);
  };

  void onDisconnect(BLEClient *pClient)
  {
    pClient->disconnect();
    deviceBleConnected = false; // clear ble connected flag
    DEBUG_MSG("I: %S disconnected \n", OWONNAME);
  }
};

bool connectToServer()
{

  DEBUG_MSG("I: Create a connection to addr: %s\n", pServerAddress->toString().c_str());

  BLEClient *pClient = BLEDevice::createClient();

  DEBUG_MSG(" - Client created\n");

  pClient->setClientCallbacks(new MyClientCallbacks());

  DEBUG_MSG(" - Connecting to server...\n");

  pClient->connect(*pServerAddress); // connect to the remove BLE Server.

  BLERemoteService *pRemoteService = pClient->getService(serviceUUID); // check if remote BLE service exists
  if (pRemoteService == nullptr)
  {
    DEBUG_MSG(" - Service not found (UUID: %s)\n", serviceUUID);
    return false;
  }
  else
  {
    DEBUG_MSG(" - Service found (UUID: %s)\n", serviceUUID);
  }

  // notify characteristic
  pRemoteCharacteristicNotify = pRemoteService->getCharacteristic(charnotificationUUID);
  if (pRemoteCharacteristicNotify == nullptr)
  {
    DEBUG_MSG(" - Notify characteristic not found (UUID: %s)\n", charnotificationUUID);
    return false;
  }
  else
  {
    DEBUG_MSG(" - Notify characteristic found (UUID: %s)\n", charnotificationUUID);
  }
  pRemoteCharacteristicNotify->registerForNotify(notifyCallback); // register notify callback
  return true;
}

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks
{

  void onResult(BLEAdvertisedDevice advertisedDevice)
  {
    DEBUG_MSG("I: BLE scan stop\n");
    if (advertisedDevice.haveName() && strcmp(advertisedDevice.getName().c_str(), OWONNAME) == 0)
    {
      advertisedDevice.getScan()->stop();
      pServerAddress = new BLEAddress(advertisedDevice.getAddress());
      DEBUG_MSG("I: BLE device found (%s at addr: %s)\n", OWONNAME, pServerAddress->toString().c_str());
    }
    else
    {
      DEBUG_MSG("I: BLE device not found\n");
    }
  }
};

void doScan()
{
  DEBUG_MSG("I: BLE scan start\n");
  startBleScanning = millis();
  BLEScan *pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->start(scanTime);
}

bool getConnectionStatus(){
  return deviceBleConnected;
}

bool newBleDataAvailable(){
  return newBleData;
}

void resetBleDataAvailable(){
  newBleData = 0;
}

unsigned long getLastBleNotifyTime(){
  return lastBleNotify;
}

void setLastBleNotifyTime(unsigned long value){
  lastBleNotify = value;
}

unsigned long getScanStartTime(){
  return startBleScanning;
}

void setScan(bool status){
  startBleScanning = status;
}

uint8_t* getReplyBuffer(){
  return replyBuffer;
}