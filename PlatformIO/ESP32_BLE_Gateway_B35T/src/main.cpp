#include <Arduino.h>
#include <ArduinoJson.h>
#include "my_debug.h"
#include "my_config.h"
#include "my_functions.h"
#include "my_wifi.h"
#ifdef MQTT
#include "my_mqtt.h"
#endif
#ifdef OTA
#include "my_ota.h"
#endif
#include "my_BLE.h"
#include "my_B35T.h"

WiFiClient wifiClient;
#ifdef MQTT
PubSubClient client(MQTT_SERVER, 1883, wifiClient); // 1883 is the listener port for the Broker
#endif
DynamicJsonDocument json(1024);

static boolean firstNotify = true; // flag first notify after start or reconnect

void setup()
{
  Serial.begin(115200);
  DEBUG_MSG("I: Start BLE Client\n");
  setup_wifi(wifiClient, MY_SSID, MY_WIFI_PASSWORD);
#ifdef MQTT
  setup_mqtt(client, MY_HOSTNAME);
#endif
#ifdef OTA
  setup_OTA(MY_HOSTNAME);
#endif
  BLEDevice::init("");
  initValues(json);
}

void loop()
{
  ArduinoOTA.handle();
  if (getConnectionStatus() == false)
  {
    delay(250);
    if (getScanStartTime() != 0 && millis() > (getScanStartTime() + (scanTime * 1000)))
    {
      setScan(0);
      return;
    }

    if (getScanStartTime() == 0)
    {
      if (firstNotify == false)
      {
        Serial.println("First Notify");
      }
      else
        doScan();
      return;
    }

    setScan(0);
    if (connectToServer())
    {
      setLastBleNotifyTime(millis());
    }
  }
  else
  {

    if (millis() > (getLastBleNotifyTime() + maxWaitForNotify) && firstNotify == false)
    {
      DEBUG_MSG("I: No notify from %s (>%d)\n", OWONNAME, maxWaitForNotify);
      return;
    }

    if (newBleDataAvailable() == true)
    {
      digitalWrite(LED_PIN, HIGH);
      parseValues(getReplyBuffer(), json);
#ifdef MQTT
      send_mqtt_json(client, MY_HOSTNAME, json, MQTT_TOPIC);
#endif
      digitalWrite(LED_PIN, LOW);
      resetBleDataAvailable();
    }
  }
}
