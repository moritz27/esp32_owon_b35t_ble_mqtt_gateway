#include "my_functions.h"

void dynamic_sleep(const uint32_t sleep_interval, uint32_t send_timestamp){
  if(send_timestamp >= sleep_interval){
    DEBUG_MSG("Restarting ESP directly");
    delay(10);
    Serial.flush();
    ESP.restart();
  }
  else{
    uint32_t sleep_time = sleep_interval - send_timestamp;
    DEBUG_MSG("Going to sleep for %d microseconds \n", sleep_time);
    Serial.flush();
    delay(10);
    #if defined(ESP8266)
      ESP.deepSleep(sleep_time);
    #elif defined(ESP32)
      esp_sleep_enable_timer_wakeup(sleep_time);
      esp_deep_sleep_start();
    #endif
    delay(10);
  }

}
