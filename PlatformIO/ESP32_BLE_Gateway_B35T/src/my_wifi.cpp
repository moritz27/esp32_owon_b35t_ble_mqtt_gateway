#include "my_wifi.h"

void setup_wifi(WiFiClient& wifi_client, const char* ssid, const char* wifi_password, const uint8_t retries) {
  WiFi.mode(WIFI_STA);
  //WiFi.setSleepMode(WIFI_NONE_SLEEP);
  //WiFi.begin(ssid, wifi_password);
  int success = false;
  DEBUG_MSG("Setup WIFI \n");
  WiFi.begin(ssid, wifi_password);
  for (int i = 0; i <= retries; i++) {
    if (WiFi.status() == WL_CONNECTED) {
      success = true;
      break;
    }
    else{
      delay(1000);
    }
  }
  if (success == true) {
    // Debugging - Output the IP Address of the ESP
    IPAddress ip = WiFi.localIP();
    DEBUG_MSG("WiFi connected, IP address: %d.%d.%d.%d \n", ip[0], ip[1], ip[2], ip[3]);
  }
  else {
    DEBUG_MSG("WIFI could not be connected, restarting now \n");
    delay(100);
    ESP.restart();
    delay(100);
  }
  return;
}