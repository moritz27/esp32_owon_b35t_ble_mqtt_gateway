void setupMQTT() {
  DEBUG_MSG("Setup MQTT ");
  while (!client.connected()) {
    Connect();
    DEBUG_MSG(".");
    delay(500);
  }
  DEBUG_MSG(".done \n");
  delay(1000);
}

bool Connect() {
  if (client.connect(my_hostname)) {
    return true;
  }
  else {
    return false;
  }
}

void sendMQTTint(int content, const char* topic) {
  char stringToSend[10];
  sprintf(stringToSend, "%i", content);
  sendMQTTstring(stringToSend, topic);
}

void sendMQTTfloat(float content, const char* topic) {
  char stringToSend[10];
  sprintf(stringToSend, " %.4f", content);
  sendMQTTstring(stringToSend, topic);
}

void sendMQTTstring(const char* content, const char* topic) {
  reconnect_to_wifi();
  if (!client.connected()) {
    DEBUG_MSG("MQTT disconnected, reconnect now");
    client.connect(my_hostname);
    delay(100);
  }
  int success = false;
  for (int i = 0; i <= RETRY_NUM; i++) {

    DEBUG_MSG("trying to send MQTT message with topic: %s", topic);

    if (client.publish(topic, content)) {
      success = true;
      break;
    }
    else {
      client.connect(my_hostname);
      delay(500);
    }
    delay(100);
  }
  if (success == true) {
    DEBUG_MSG(" ..done \n");
  }
  else {
    DEBUG_MSG(" ..message could not be sent, restarting now");
    delay(100);
    ESP.restart();
    delay(100);
  }
}
