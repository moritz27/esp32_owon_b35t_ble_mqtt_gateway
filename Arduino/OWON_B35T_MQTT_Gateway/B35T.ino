//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*
  14 bytes reply
  -----------------------------------------------------------------------
  |  1 |         2         |  3 |  4 |  5 |  6 |    7    |  8 |    9    |
  -----------------------------------------------------------------------
  |0x2b|0x33 0x36 0x32 0x33|0x20|0x34|0x31|0x00|0x40 0x80|0x24|0x0d 0x0a|
  -----------------------------------------------------------------------
  |  0 |  1    2    3    4 |  5 |  6 |  7 |  8 |  9   10 | 11 | 12   13 |
  -----------------------------------------------------------------------
*/

/*
  1:  + or - (dec 43 or 45)
    BYTE 0
*/
#define REGPLUSMINUS    0x00
#define FLAGPLUS        B00101011
#define FLAGMINUS       B00101101

/*
  2:  Value 0000-9999 in dec
    BYTE 1-4
*/
#define REGDIG1         0x01
#define REGDIG2         0x02
#define REGDIG3         0x03
#define REGDIG4         0x04

/*
  3:  Just space (dec 32)
    BYTE 5
*/

/*
  4:  Decimal point position
    - dec 48 no point
    - dec 49 after the first number
    - dec 50 after the second number
    - dec 52 after the third number
    BYTE 6
*/
#define REGPOINT        0x06
#define FLAGPOINT0      B00110000
#define FLAGPOINT1      B00110001
#define FLAGPOINT2      B00110010
#define FLAGPOINT3      B00110100

/*
  5:  AC or DC and Auto mode
    - dec 49 DC Auto mode / 51 HOLD
    - dec 41 AC Auto mode / 43 HOLD
    - dec 17 DC Manual mode / 19 HOLD
    - dec 09 AC Manual mode / 11 HOLD
    BYTE 7
*/
#define REGMODE         0x07
#define FLAGMODENONE    B00000000   //none
#define FLAGMODEMAN     B00000001   //manual
#define FLAGMODEHOLD    B00000010   //hold
#define FLAGMODEREL     B00000100   //relative  
#define FLAGMODEAC      B00001000   //ac
#define FLAGMODEDC      B00010000   //dc
#define FLAGMODEAUTO    B00100000   //auto
#define FLAGMODECHECK   B11111111   //mask for check others

/*
  6:  MIN MAX
    - dec  0 MIN MAX off
    - dec 16 MIN
    - dec 32 MAX
    BYTE 8
*/
#define REGMINMAX       0x08
#define FLAGMINMAXNONE  B00000000
#define FLAGMIN         B00010000
#define FLAGMAX         B00100000

/*
  7:  Units
    - dec   2   0    % Duty
    - dec   0   1 Fahrenheit
    - dec   0   2 Grad
    - dec   0   4   nF
    - dec   0   8   Hz
    - dec   0  16  hFE
    - dec   0  32  Ohm
    - dec  32  32 kOhm
    - dec  16  32 MOhm
    - dec 128  64   uA
    - dec  64  64   mA
    - dec   0  64    A
    - dec  64 128   mV
    - dec   0 128    V
    BYTES 9 & 10
*/
#define REGSCALE        0x09
#define FLAGSCALEDUTY   B00000010
#define FLAGSCALEDIODE  B00000100
#define FLAGSCALEBUZZ   B00001000
#define FLAGSCALEMEGA   B00010000
#define FLAGSCALEKILO   B00100000
#define FLAGSCALEMILI   B01000000
#define FLAGSCALEMICRO  B10000000

#define REGUNIT         0x0a
#define FLAGUNITNONE    B00000000
#define FLAGUNITFAHR    B00000001
#define FLAGUNITGRAD    B00000010
#define FLAGUNITNF      B00000100
#define FLAGUNITHZ      B00001000
#define FLAGUNITHFE     B00010000
#define FLAGUNITOHM     B00100000
#define FLAGUNITAMP     B01000000
#define FLAGUNITVOLT    B10000000

/*
  8:  ???
*/

/*
  9:  CR + LF
*/
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

String checkUnit(char* unitByte, String lastUnit) {
  if (*unitByte != meterReplyBuffer[REGUNIT]) {
    *unitByte = meterReplyBuffer[REGUNIT];
    DEBUG_MSG("Unit Byte: %#08x\n", *unitByte);
    switch (*unitByte) {
      case FLAGUNITFAHR:
        return "*F";
        break;
      case FLAGUNITGRAD:
        return "*C";
        break;
      case FLAGUNITNF:
        return "nF";
        break;
      case FLAGUNITHZ:
        return "Hz";
        break;
      case FLAGUNITHFE:
        return "hFE";
        break;
      case FLAGUNITOHM:
        return "Ohm";
        break;
      case FLAGUNITAMP:
        return "A";
        break;
      case FLAGUNITVOLT:
        return "V";
        break;
        break;
    }
  }
  return lastUnit;
}

String checkScale(char* scaleByte, String lastScale) {
  if (*scaleByte != meterReplyBuffer[REGSCALE]) {
    if ((meterReplyBuffer[REGSCALE] & FLAGSCALEDUTY) == FLAGSCALEDUTY) {
      *scaleByte |= FLAGSCALEDUTY;
      return "%";
    } else {
      *scaleByte &= ~(FLAGSCALEDUTY);
    }

    if ((meterReplyBuffer[REGSCALE] & FLAGSCALEDIODE) == FLAGSCALEDIODE)
      *scaleByte |= FLAGSCALEDIODE;
    else
      *scaleByte &= ~(FLAGSCALEDIODE);

    if ((meterReplyBuffer[REGSCALE] & FLAGSCALEBUZZ) == FLAGSCALEBUZZ) {
      *scaleByte |= FLAGSCALEBUZZ;
    } else {
      *scaleByte &= ~(FLAGSCALEBUZZ);
    }

    if ((meterReplyBuffer[REGSCALE] & FLAGSCALEMEGA) == FLAGSCALEMEGA) {
      *scaleByte |= FLAGSCALEMEGA;
      return "M";
    } else {
      *scaleByte &= ~(FLAGSCALEMEGA);
    }

    if ((meterReplyBuffer[REGSCALE] & FLAGSCALEKILO) == FLAGSCALEKILO) {
      *scaleByte |= FLAGSCALEKILO;
      return "k";
    } else {
      *scaleByte &= ~(FLAGSCALEKILO);
    }

    if ((meterReplyBuffer[REGSCALE] & FLAGSCALEMILI) == FLAGSCALEMILI) {
      *scaleByte |= FLAGSCALEMILI;
      return "m";
    } else {
      *scaleByte &= ~(FLAGSCALEMILI);
    }

    if ((meterReplyBuffer[REGSCALE] & FLAGSCALEMICRO) == FLAGSCALEMICRO) {
      *scaleByte |= FLAGSCALEMICRO;
      return "u";
    } else {
      *scaleByte &= ~(FLAGSCALEMICRO);
    }
    return "";
  }
  return lastScale;
}

String checkMeasMode(char* modeByte, String lastMode) {

  if (*modeByte != meterReplyBuffer[REGMODE]) {
    String measMode = "";
    if ((*modeByte & FLAGMODEAUTO) != (meterReplyBuffer[REGMODE] & FLAGMODEAUTO)) {  // auto
      if ((meterReplyBuffer[REGMODE] & FLAGMODEAUTO) == FLAGMODEAUTO) {
        *modeByte |= FLAGMODEAUTO;
        measMode += "Auto";
        doc["capture"] = "Auto";
      }
      else {
        *modeByte &= ~(FLAGMODEAUTO);
      }
      DEBUG_MSG("Auto Mode: %s\n", (*modeByte & FLAGMODEAUTO) ? "true" : "false");
    }
    if ((*modeByte & FLAGMODEHOLD) != (meterReplyBuffer[REGMODE] & FLAGMODEHOLD)) {  // hold
      if ((meterReplyBuffer[REGMODE] & FLAGMODEHOLD) == FLAGMODEHOLD) {
        *modeByte |= FLAGMODEHOLD;
        measMode += "Hold";
        doc["capture"] = "Hold";
      }
      else {
        *modeByte &= ~(FLAGMODEHOLD);
      }
      DEBUG_MSG("Hold: %s\n", (*modeByte & FLAGMODEHOLD) ? "true" : "false");
    }
    if ((*modeByte & FLAGMODEREL) != (meterReplyBuffer[REGMODE] & FLAGMODEREL)) {  // relative
      if ((meterReplyBuffer[REGMODE] & FLAGMODEREL) == FLAGMODEREL) {
        *modeByte |= FLAGMODEREL;
        measMode += "Relative";
        doc["capture"] = "Relative";
      }
      else {
        *modeByte &= ~(FLAGMODEREL);
      }
      DEBUG_MSG("Relative: %s\n", (*modeByte & FLAGMODEREL) ? "true" : "false");
    }
    if ((*modeByte & FLAGMODEDC) != (meterReplyBuffer[REGMODE] & FLAGMODEDC)) {  // dc
      if ((meterReplyBuffer[REGMODE] & FLAGMODEDC) == FLAGMODEDC) {
        *modeByte |= FLAGMODEDC;
        measMode += " DC";
        doc["coupling"] = "DC";
      }
      else {
        *modeByte &= ~(FLAGMODEDC);
      }
      DEBUG_MSG("DC: %s\n", (*modeByte & FLAGMODEDC) ? "true" : "false");
    }
    if ((*modeByte & FLAGMODEAC) != (meterReplyBuffer[REGMODE] & FLAGMODEAC)) {  // ac
      if ((meterReplyBuffer[REGMODE] & FLAGMODEAC) == FLAGMODEAC) {
        *modeByte |= FLAGMODEAC;
        measMode += " AC";
        doc["coupling"] = "AC";
      }
      else {
        *modeByte &= ~(FLAGMODEAC);
      }
      DEBUG_MSG("AC: %s\n", (*modeByte & FLAGMODEAC) ? "true" : "false");
    }
    return measMode;
  }
  return lastMode;
}

String checkMinMax(char* minMaxByte, String lastMiniMax) {
  if (*minMaxByte != meterReplyBuffer[REGMINMAX]) {
    String minMax = "No";
    if ((meterReplyBuffer[REGMINMAX] & FLAGMAX) == FLAGMAX) {
      *minMaxByte |= FLAGMAX;
      DEBUG_MSG("Max: %s\n", (*minMaxByte & FLAGMAX) ? "true" : "false");
      minMax = "Max";
    }
    else {
      *minMaxByte &= ~(FLAGMAX);
    }
    if ((meterReplyBuffer[REGMINMAX] & FLAGMIN) == FLAGMIN) {
      *minMaxByte |= FLAGMIN;
      DEBUG_MSG("Min: %s\n", (*minMaxByte & FLAGMIN) ? "true" : "false");
      minMax = "Min";
    }
    else {
      *minMaxByte &= ~(FLAGMIN);
    }
    return minMax;
  }
  return lastMiniMax;
}

float convertValue(char* plusMinusByte, char* dig1, char* dig2, char* dig3, char* dig4, char* pointByte) {
  char digits[6] = {0, 0, 0, 0, 0, 0};
  uint8_t digitOffset = 0;

  if (*plusMinusByte != meterReplyBuffer[REGPLUSMINUS]) {                      // sign "-" : 0
    *plusMinusByte = meterReplyBuffer[REGPLUSMINUS];
  }
  if ((*plusMinusByte & FLAGMINUS) == FLAGMINUS) {
    digits[0] = '-';
    digitOffset++;
  }

  if (*dig1 != meterReplyBuffer[REGDIG1]) {                                // *digit 1 : 0/1
    *dig1 = meterReplyBuffer[REGDIG1];
  }
  if (*dig1 >= 0x30 && *dig1 <= 0x39) {
    digits[0 + digitOffset] = *dig1;
  }

  if (*pointByte != meterReplyBuffer[REGPOINT]) {                            // decimal point
    *pointByte = meterReplyBuffer[REGPOINT];
  }

  if ((*pointByte & FLAGPOINT1) == FLAGPOINT1) {                            // decimal point 1 : 1/2
    digits[1 + digitOffset] = '.';
    digitOffset++;
  }

  if (*dig2 != meterReplyBuffer[REGDIG2]) {
    *dig2 = meterReplyBuffer[REGDIG2];
  }
  if (*dig2 >= 0x30 && *dig2 <= 0x39) {
    digits[1 + digitOffset] = *dig2;                                   // *digit 2 : 1/2/3
  }

  if (((*pointByte & FLAGPOINT2) == FLAGPOINT2) && (digitOffset <= 1)) {        // decimal point 2 : 2/3
    digits[2 + digitOffset] = '.';
    digitOffset++;
  }

  if (*dig3 != meterReplyBuffer[REGDIG3]) {
    *dig3 = meterReplyBuffer[REGDIG3];
  }
  if (*dig3 >= 0x30 && *dig3 <= 0x39) {
    digits[2 + digitOffset] = *dig3;                                   // *digit 3 : 2/3/4
  }

  if (((*pointByte & FLAGPOINT3) == FLAGPOINT3) && (digitOffset <= 1)) {        // decimal point 3/4
    digits[3 + digitOffset] = '.';
    digitOffset++;
  }

  if (*dig4 != meterReplyBuffer[REGDIG4]) {
    *dig4 = meterReplyBuffer[REGDIG4];
  }

  if (*dig4 >= 0x30 && *dig4 <= 0x39) {
    digits[3 + digitOffset] = *dig4;                                    // *digit 3/4/5
  }

  float numValue = atof(digits);
  return numValue;
}

String actualScale = "";
String actualUnit = "V";
String actualMode = "Auto DC";
String actualMinMax = "No";

float normalizeValue(float measurement, String usedScale) {
  if (usedScale == "M") {
    return measurement * 1e6;
  }
  else if (usedScale == "k") {
    return measurement * 1e3;
  }
  else if (usedScale == "m") {
    return measurement / 1e3;
  }
  else if (usedScale == "u") {
    return measurement / 1e6;
  }
  else {
    return measurement;
  }
}

void parseValues() {
  static char valuetmp[meterReplySize - 3];
  actualScale = checkScale(&valuetmp[REGSCALE], actualScale);
  actualUnit = checkUnit(&valuetmp[REGUNIT], actualUnit);
  actualMode = checkMeasMode(&valuetmp[REGMODE], actualMode);
  actualMinMax = checkMinMax(&valuetmp[REGMINMAX], actualMinMax);
  float measuredValue = convertValue(&valuetmp[REGPLUSMINUS], &valuetmp[REGDIG1], &valuetmp[REGDIG2], &valuetmp[REGDIG3], &valuetmp[REGDIG4], &valuetmp[REGPOINT]);
  float normalizedValue = normalizeValue(measuredValue, actualScale);
  //  Serial.printf("%4.3f %s%s \n", measuredValue, actualScale, actualUnit);
  //  Serial.printf("%E %s \n", normalizedValue, actualUnit);

  DEBUG_MSG("Actual Mode: %s \n", actualMode);

  doc["value"] = normalizedValue;
  doc["unit"] = actualUnit;
  //  doc["mode"] = actualMode;
  doc["minMax"] = actualMinMax;

  newBleData = false;
}
