void setupWIFI() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, wifi_password);
  Serial.print("Setup WIFI: ");
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    WiFi.begin(ssid, wifi_password);
    DEBUG_MSG("Retrying connection...\n");
    delay(500);
  }
  // Wait until the connection has been confirmed before continuing
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    DEBUG_MSG(".");
  }
  DEBUG_MSG("..done ");
}

void reconnect_to_wifi() {
  if (WiFi.status() != WL_CONNECTED) {
    DEBUG_MSG("reconnect to wifi now");
    WiFi.reconnect();
    while (WiFi.status() != WL_CONNECTED) {
      DEBUG_MSG(".");
      delay(500);
    }
    DEBUG_MSG("\n");
  }
  delay(500);
  return;
}
