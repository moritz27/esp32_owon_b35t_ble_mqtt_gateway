#include <Arduino.h>
#include <WiFi.h>
#include <Wire.h>
#include <PubSubClient.h> 
#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <BLEDevice.h>
#include <BLEAdvertisedDevice.h>
#include <BLEClient.h>
#include <BLEScan.h>
#include <BLEUtils.h>


/* ########################## DEBUG ########################## */
#define MYDEBUG
#ifdef MYDEBUG
#define DEBUG_MSG(...) Serial.printf( __VA_ARGS__ )
#else
#define DEBUG_MSG(...)
#endif

/* ########################## WIFI ########################## */
const char* ssid = "SchneW";
const char* wifi_password = "uhTDhtZu*z9U@gK%";
WiFiClient wifiClient;

/* ########################## MQTT ########################## */
#define MQTT_KEEPALIVE 1
#define RETRY_NUM 5
const char* mqtt_server = "192.168.178.11";
const char* mqtt_topic = "/B35T";
const char* my_hostname = "B35T_MQTT_Gateway";
PubSubClient client(mqtt_server, 1883, wifiClient); // 1883 is the listener port for the Broker

/* ########################## user variables ########################## */
#define LED_PIN 2
#define SEND_CYLCE_TIME 10 // 10 seconds
uint32_t timer = millis();
DynamicJsonDocument doc(1024);


/* ########################## OWON B35T ########################## */
const char* OWONNAME = "BDM";                                                   // OWON device name
static BLEUUID serviceUUID("0000fff0-0000-1000-8000-00805f9b34fb");             // OWON service UUID
static BLEUUID charnotificationUUID("0000fff4-0000-1000-8000-00805f9b34fb");    // OWON notification characteristic UUID
static BLEUUID charwriteUUID("0000fff3-0000-1000-8000-00805f9b34fb");           // OWON write characteristic UUID

static BLEAddress *pServerAddress;
static BLERemoteCharacteristic* pRemoteCharacteristicNotify;
static BLERemoteCharacteristic* pRemoteCharacteristicWrite;

volatile boolean deviceBleConnected = false;                                    // flag BLE conneted
volatile boolean newBleData = false;                                            // flag "we get new data from meter"
static unsigned long lastBleNotify = 0;                                         // timestamp "last received data from meter" in ms
static unsigned long startBleScanning = 0;                                      // timestamp when ble scan is beginning in ms

const uint16_t maxWaitForNotify = 10000;                                        // max wait time for next notify from meter in ms
const uint16_t scanTime = 10;                                                   // BLE scan time in s

const uint8_t meterReplySize = 14;                                              // number of bytes in meter reply
char meterReplyBuffer[meterReplySize];                                                 // meter reply buffer

static boolean firstNotify = true;                                              // flag first notify after start or reconnect

void setup() {
  Serial.begin(115200);
  DEBUG_MSG("I: Start OWON B35T Client\n");
  setupWIFI();
  setupMQTT();
  setupOTA();
  BLEDevice::init("");
}

void loop() {
  ArduinoOTA.handle();
  if (deviceBleConnected == false) {
    delay(250);
    if (startBleScanning != 0 && millis() > (startBleScanning + (scanTime * 1000))) {
      startBleScanning = 0;
      return;
    }

    if (startBleScanning == 0) {
      if (firstNotify == false) {
        Serial.println("First Notify");
      }
      else
        doScan();
      return;
    }

    startBleScanning = 0;
    if (connectToServer(*pServerAddress)) {
      lastBleNotify = millis();
    }

  } else {

    if (millis() > (lastBleNotify + maxWaitForNotify) && firstNotify == false) {
      DEBUG_MSG("I: No notify from %s (>%d)\n", OWONNAME, maxWaitForNotify);
      return;
    }

    if (newBleData == true) {
      digitalWrite(LED_PIN,HIGH);
      parseValues();
      String json_string;
      serializeJson(doc, json_string);
      DEBUG_MSG("%s \n", json_string.c_str());
      sendMQTTstring(json_string.c_str(),mqtt_topic);
      digitalWrite(LED_PIN,LOW);
    }
  }
}
